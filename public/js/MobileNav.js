"use strict";

const MobileNav = (function() {

	this.DOM = {
		toggle : document.getElementById('MobileNav.toggle'),
		header : document.getElementById('MobileNav.header'),
		content : document.getElementById('MobileNav.content')
	};


	this.DOM.header.addEventListener('click', evt => {

		this.DOM.toggle.classList.toggle('open');

	});

}).apply({});
