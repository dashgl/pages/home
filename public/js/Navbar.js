const NavBar = (function() {

	this.MEM = {}

	this.DOM = {
		table : document.getElementById('Navbar.table'),
		td : {
			hello : document.getElementById('Navbar.td.hello'),
			brickout : document.getElementById('Navbar.td.brickout'),
			invaders : document.getElementById('Navbar.td.invaders'),
			astroids : document.getElementById('Navbar.td.astroids')
		},
		nav : {
			desktop : document.getElementById('Navbar.nav.desktop'),
			mobile : document.getElementById('MobileNav.toggle'),
			banner : document.getElementById('Navbar.nav.banner')
		},
		content : {
			body : document.getElementById('NavBar.content.body'),
			title : document.getElementById('NavBar.content.title'),
			created_on : document.getElementById('NavBar.content.created_on'),
			last_edit : document.getElementById('NavBar.content.last_edit'),
			author : document.getElementById('NavBar.content.author')
		}
	}
	
	this.EVT = {
		handleTableClick : evt_handleTableClick.bind(this),
		handleLinkClick : evt_handleLinkClick.bind(this),
		handlePopState : evt_handlePopState.bind(this)
	}

	this.API = {
		parseQuery : api_parseQuery.bind(this),
		renderList : api_renderList.bind(this),
		renderPage : api_renderPage.bind(this),
		renderHeader : api_renderHeader.bind(this),
		openLink : api_openLink.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		// this.DOM.table.addEventListener('click', this.EVT.handleTableClick);

		window.onpopstate = this.EVT.handlePopState;

		// If Javascript is enabled, then we loop over all of relative links
		// within the site so that we can load them with ajax instead of with
		// standard http requests

		const links = document.getElementsByTagName("a");
		for(let i = 0; i < links.length; i++) {
			if(links[i].getAttribute("target")) {
				continue;
			}
			links[i].addEventListener('click', this.EVT.handleLinkClick);
		}
		
		// Check to see if php if active on this page
		// If it is active, then we can assume the page has already
		// been rendered and we don't need to trigger Javascript

		// If we don't detect php, then we need to get the
		// current url and render the current page for the user

		this.API.openLink(window.location.search || "");

	}

	function evt_handlePopState(evt) {
		
		evt.preventDefault();
		console.log(evt.state);


	}

	function api_openLink(src, pushState) {

		// First we parse the url of the intended 

		const query = this.API.parseQuery(src);

		// Then we add checks for if the topic or article
		// is not set then we set first element from ToC

		if(!query.topic) {
			query.topic = Object.keys(Table)[0];
		}

		if(!query.article) {
			query.article = Object.keys(Table[query.topic])[0];
		}

		// Then we update the user interface with the requested
		// article and topic selected by the user

		this.API.renderList(query);
		this.API.renderPage(query);
		this.API.renderHeader(query);

		// Save the state of the query to the module

		this.MEM.query = query;

		// If pushState is not set then we skip adding to the 
		// history object for the popstate event

		if(!pushState) {
			return;
		}
		
		console.log("pushing state!!!");
		const details = Table[query.topic][query.article];
		history.pushState(query, details.title, src);

	}

	function api_renderHeader(query) {
		
		console.log("hey, hey, we update the header");
		console.log(this.DOM);

		this.MEM.query = this.MEM.query || {};
		if(this.MEM.query.topic === query.topic) {
			return;
		}

		this.DOM.nav.desktop.setAttribute("class", query.topic);

		if(this.MEM.activeTD) {
			this.MEM.activeTD.classList.remove("active");
		}

		this.MEM.activeTD = this.DOM.td[query.topic];

		if(this.MEM.activeTD) {
			this.MEM.activeTD.classList.add("active");
		}

	}

	function api_renderList(query) {

		console.log("render list!!");
		console.log(query);

		// First if the topic doesn't match, we need to create a new list

		if(query.topic === this.MEM.topic) {

		}

		// Then we need to set which elements should be active based on 
		// which article has been selected

	}

	function api_renderPage(query, topic) {

		console.log("render page!!!");
		const url = "docs/"+query.topic+"/"+query.article+".md";
		console.log(url);

		const ajax = new XMLHttpRequest();
		ajax.open("GET", url);
		ajax.send();

		const details = Table[query.topic][query.article];
		console.log(details);

		ajax.onload = () => {
			
			const text = ajax.responseText;
			const converter = new showdown.Converter();
			this.DOM.content.body.innerHTML = converter.makeHtml(text);
			this.DOM.content.title.textContent = details.title;
			this.DOM.content.author.textContent = details.author;
			this.DOM.content.last_edit.textContent = details.last_edit;
			this.DOM.content.created_on.textContent = details.created_on;

			scrollTo(this.DOM.content.title);

		}

	}

	function api_parseQuery(queryString) {
		let query = {};
		let pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
		for (let i = 0; i < pairs.length; i++) {
			let pair = pairs[i].split('=');
			query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
		}
		return query;
	}

	function evt_handleLinkClick(evt) {
		
		evt.preventDefault();
		const link = evt.target;
		let src = link.getAttribute("href");
		this.API.openLink(src, true);	
		console.log("handle link click!!");

	}

	function evt_handleTableClick(evt) {

		let elem = evt.target;
		while(elem.parentNode && elem.tagName !== "TD") {
			elem = elem.parentNode;
		}

		if(!elem.getAttribute) {
			return;
		}

		let id = elem.getAttribute("id");
		let leaf = id.split(".").pop();

		if(this.DOM.td[leaf].classList.contains("active")) {
			return;
		}

		for(let key in this.DOM.td) {
			this.DOM.td[key].classList.remove("active");
		}

		this.DOM.td[leaf].classList.add("active");
		this.DOM.nav.desktop.setAttribute("class", leaf);
		this.DOM.nav.mobile.setAttribute("class", "mobile-nav " + leaf);

	}

}).apply({});
